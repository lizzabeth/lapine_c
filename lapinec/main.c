#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include "lapine.h"

int main(int argc, char* argv[]) {
    if (argc < 2) {
        printf("usage: lapinec <file...>\n");
        return EXIT_SUCCESS;
    }

    FILE* fp = fopen(argv[1], "r");
    if (!fp) {
        printf("couldn't load file %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    fseek(fp, 0, SEEK_END);
    long len = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    char* src = calloc(len, 1);
    fread(src, 1, len, fp);
    fclose(fp);

    lexer l;
    lexer_init(&l, src);

    token t;
    do {
        t = lexer_next(&l);
        if (t.type >= TOK_ERROR) {
            printf("error at (%" PRIuPTR ",%" PRIuPTR ")\n", l.lin, l.col);
            return EXIT_FAILURE;
        }
        char* tok = token_show(t);
        printf("%s\n", tok);
        free(tok);
    } while(t.type != TOK_EOF);

    return EXIT_SUCCESS;
}

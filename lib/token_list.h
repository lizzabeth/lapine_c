#pragma once

#define TOKEN_LIST \
    X(IDENT) \
    X(MODULE) \
    X(LET) \
    X(FUN) \
    X(LPAREN) \
    X(RPAREN) \
    X(LBRACK) \
    X(RBRACK) \
    X(LBRACE) \
    X(RBRACE) \
    X(COLON) \
    X(SEMI) \
    X(COMMA) \
    X(ADD) \
    X(SUB) \
    X(MUL) \
    X(DIV) \
    X(ADDEQ) \
    X(SUBEQ) \
    X(MULEQ) \
    X(DIVEQ) \
    X(EQ) \
    X(EQEQ) \
    X(NEQ) \
    X(NOT) \
    X(GT) \
    X(LT) \
    X(GTEQ) \
    X(LTEQ)

#pragma once
#include "token_list.h"

typedef enum token_type {
    #define X(name) TOK_##name,
    TOKEN_LIST
    #undef X
    TOK_EOF, TOK_ERROR
} token_type;

typedef struct token {
    token_type type;
    union {
        char* sval;
    };
} token;

const char* token_name(token_type t);
char*       token_show(token t);
token_type  token_kwd(const char* k);


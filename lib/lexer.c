#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include "lexer.h"

static void     next(lexer* l);
static void     line(lexer* l);
static token    accept(lexer* l, token_type t);
static token    follow(lexer* l, char c, token_type a, token_type b);
static void     scomment(lexer* l);
static token    ident(lexer* l);

void lexer_init(lexer* l, char* src) {
    assert(l);

    l->lin = 1;
    l->col = 1;
    l->src = src;
    l->cur = src;
    l->beg = src;
    buffer_init(&l->buf, 16);
}

token lexer_next(lexer* l) {
    assert(l && l->src);

    while(1) {
        char c = *l->cur;
        switch(c) {
        case 0:     return (token) { .type = TOK_EOF };
        // Whitespace
        case ' ':   next(l); break;
        case '\n':  next(l); line(l); break;
        case '#':   next(l); scomment(l); break;
        // Punctuation
        case '(':   return accept(l, TOK_LPAREN);
        case ')':   return accept(l, TOK_RPAREN);
        case '[':   return accept(l, TOK_LBRACK);
        case ']':   return accept(l, TOK_RBRACK);
        case '{':   return accept(l, TOK_LBRACE);
        case '}':   return accept(l, TOK_RBRACE);
        case ':':   return accept(l, TOK_COLON);
        case ';':   return accept(l, TOK_SEMI);
        case ',':   return accept(l, TOK_COMMA);
        // Operators 
        case '+':   return follow(l, '=', TOK_ADD, TOK_ADDEQ);
        case '-':   return follow(l, '=', TOK_SUB, TOK_SUBEQ);
        case '*':   return follow(l, '=', TOK_MUL, TOK_MULEQ);
        case '/':   return follow(l, '=', TOK_DIV, TOK_DIVEQ);
        case '=':   return follow(l, '=', TOK_EQ, TOK_EQEQ);
        case '!':   return follow(l, '=', TOK_NOT, TOK_NEQ);
        case '>':   return follow(l, '=', TOK_GT, TOK_GTEQ);
        case '<':   return follow(l, '=', TOK_LT, TOK_LTEQ);
        default:
            if (isalpha(c)) return ident(l);
            return (token) { .type = TOK_ERROR };
        }
    }
}

static void next(lexer* l) {
    l->cur++;
    l->col++;
}

static void line(lexer* l) {
    l->lin++;
    l->col = 1;
    l->beg = l->cur;
}

static token accept(lexer* l, token_type t) {
    next(l);
    return (token) { .type = t };
}

static token follow(lexer* l, char c, token_type a, token_type b) {
    next(l);
    if (*l->cur == c) {
        next(l);
        return (token) { .type = b };
    } 
    return (token) { .type = a };
}

static void scomment(lexer* l) {
    while(*l->cur != 0 && *l->cur != '\n') next(l);
    if (*l->cur == '\n') {
        next(l);
        line(l);
    }
}

static token ident(lexer* l) {
    while(isalnum(*l->cur) || *l->cur == '_') {
        buffer_push(&l->buf, *l->cur);
        next(l);
    }

    char* str = buffer_string(&l->buf);
    buffer_zero(&l->buf);

    token_type t = token_kwd(str);
    if (t == TOK_ERROR) {
        return (token) { .type = TOK_IDENT, .sval = str };
    } else {
        return (token) { .type = t };
    }
}

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "buffer.h"

void buffer_init(buffer* b, size_t cap) {
    assert(b);

    b->len = 0;
    b->cap = cap;
    b->ptr = calloc(cap, 1);
}

void buffer_free(buffer* b) {
    assert(b && b->ptr);

    free(b->ptr);
    b->len = 0;
    b->cap = 0;
}

void buffer_zero(buffer* b) {
    assert(b && b->ptr);

    b->len = 0;
    memset(b->ptr, 0, b->len);
}

void buffer_push(buffer* b, char c) {
    assert(b && b->ptr);

    if (b->len == b->cap) buffer_grow(b, b->cap * 2);
    b->ptr[b->len++] = c;
}

void buffer_grow(buffer* b, size_t cap) {
    assert(b && b->ptr);
    assert(cap > b->cap);

    b->ptr = realloc(b->ptr, cap);
    memset(b->ptr + b->len, 0, cap - b->cap);
    b->cap = cap;
}

char* buffer_string(buffer* b) {
    assert(b && b->ptr && b->len);
    char* str = calloc(b->len+1, 1);
    memcpy(str, b->ptr, b->len);
    return str;
}

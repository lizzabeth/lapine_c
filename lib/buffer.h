#pragma once
#include <stddef.h>

typedef struct buffer {
    size_t  len;
    size_t  cap;
    char*   ptr;
} buffer;

void    buffer_init(buffer* b, size_t cap);
void    buffer_free(buffer* b);
void    buffer_zero(buffer* b);
void    buffer_push(buffer* b, char c);
void    buffer_grow(buffer* b, size_t cap);
char*   buffer_string(buffer* b);

#pragma once
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include "buffer.h"
#include "token.h"

typedef struct lexer {
    uintptr_t   lin;
    uintptr_t   col;
    char*       src;
    char*       cur;
    char*       beg;
    buffer      buf;
} lexer;

void    lexer_init(lexer* l, char* src);
token   lexer_next(lexer* l);

file(GLOB sources *.c)
add_library(lapine STATIC ${sources})
target_include_directories(lapine PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})


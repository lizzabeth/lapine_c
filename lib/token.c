#include <stdio.h>
#include <string.h>
#include "token.h"

const struct { char* name; token_type tok; } kwd_table[3] = {
    { "module", TOK_MODULE },
    { "let",    TOK_LET },
    { "fun",    TOK_FUN },
};

const char* token_names[TOK_ERROR] = {
    #define X(name) [TOK_##name] = #name,
    TOKEN_LIST
    #undef X
    [TOK_EOF]       = "EOF",
};

const char* token_name(token_type t) {
    if (t < TOK_ERROR) return token_names[t];
    return "invalid";
}

char* token_show(token t) {
    char* res;
    switch(t.type) {
    case TOK_IDENT: asprintf(&res, "ident(%s)", t.sval); break;
    default:
        if (t.type < TOK_ERROR) asprintf(&res, "%s", token_name(t.type));
        else asprintf(&res, "INVALID");
    }

    return res;
}

token_type token_kwd(const char* k) {
    for (int i = 0; i < 3; i++)
        if (strcmp(k, kwd_table[i].name) == 0)
            return kwd_table[i].tok;
    return TOK_ERROR;
}
